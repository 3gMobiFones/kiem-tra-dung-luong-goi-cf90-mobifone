# kiem-tra-dung-luong-goi-cf90-mobifone
3 Cách kiểm tra dung lượng gói CF90 MobiFone thành công
<p style="text-align: justify;">Bạn đã biết <a href="https://3gmobifones.com/cach-kiem-tra-dung-luong-goi-cf90-mobifone"><strong>cách tra cứu dung lượng còn lại của gói CF90 MobiFone</strong></a> chưa? Đây là gói cước ưu đãi 50GB dùng trong 1 tháng, vì vậy bạn cần phải thực hiện kiểm tra dung lượng để xem được đã sử dụng bao nhiêu data, còn lại bao nhiêu data.</p>
<p style="text-align: justify;">Có thể thấy rằng, thao tác tra cứu data là một trong những bước quan trọng mà người dùng viễn thông nào cũng nên nắm rõ. Đối với gói cước CF90 bạn cần kiểm tra bằng những cách sau đây mà <a href="http://3gmobifones.com" target="_blank" rel="noopener">3gmobifones.com</a> chia sẻ.</p>
